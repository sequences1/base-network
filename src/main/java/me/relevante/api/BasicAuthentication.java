package me.relevante.api;

import me.relevante.network.Network;

import java.util.Base64;

public class BasicAuthentication<N extends Network> {

    private String authItem;

    public BasicAuthentication(String username,
                               String password) {
        String encoding = username + ":" + password;
        this.authItem = Base64.getEncoder().encodeToString(encoding.getBytes());
    }

    public String getAuthItem() {
        return authItem;
    }
}
