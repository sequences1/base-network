package me.relevante.api;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkProfile;
import me.relevante.network.Network;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public interface NetworkApi<N extends Network, T extends NetworkProfile> extends NetworkEntity<N> {
    <U extends Exception> T getUserData() throws U;
}
