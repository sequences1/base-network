package me.relevante.api;

import me.relevante.network.Network;import java.lang.String;

public class OAuthKeyPair<N extends Network> {

    protected String key;
    protected String secret;

    public OAuthKeyPair(String key, String secret) {
        this.key = key;
        this.secret = secret;
    }

    public String getKey() {
        return key;
    }

    public String getSecret() {
        return secret;
    }
}
