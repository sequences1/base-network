package me.relevante.nlp;

import me.relevante.model.NetworkSignal;
import me.relevante.model.SignalCategory;
import me.relevante.network.Network;
import me.relevante.nlp.core.Feature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public abstract class AbstractNetworkVectorizer<N extends Network> implements NetworkVectorizer<N> {

    private Map<Class, Function<Double, Double>> weightFunctionsByClass;

    public AbstractNetworkVectorizer() {
        this.weightFunctionsByClass = createWeightFunctionsBySignal();
    }

    @Override
    public Map<String, Feature> generateVector(List<NetworkSignal<N>> signals) {

        Map<String, List<NetworkSignal<N>>> signalsByStem = generateSignalsByStem(signals);

        Map<String, Feature> vector = new HashMap<>();
        for (Map.Entry<String, List<NetworkSignal<N>>> entry : signalsByStem.entrySet()) {
            String stem = entry.getKey();
            List<NetworkSignal<N>> stemSignals = entry.getValue();
            double score = calculateStemSignalsScore(stemSignals);
            Feature feature = new Feature(stem, score);
            vector.put(stem, feature);
        }
        return vector;
    }

    private Map<String, List<NetworkSignal<N>>> generateSignalsByStem(List<NetworkSignal<N>> signals) {
        Map<String, List<NetworkSignal<N>>> signalsByStem = new HashMap<>();
        signals.forEach(signal -> addSignalKeywords(signal, signalsByStem));
        return signalsByStem;
    }


    private void addSignalKeywords(NetworkSignal<N> signal, Map<String, List<NetworkSignal<N>>> signalsByStem) {
        for (String stem : signal.getContentStems()) {
            List<NetworkSignal<N>> stemsSignals = signalsByStem.get(stem);
            if (stemsSignals == null) {
                stemsSignals = new ArrayList<>();
                signalsByStem.put(stem, stemsSignals);
            }
            stemsSignals.add(signal);
        }
    }

    private double calculateStemSignalsScore(List<NetworkSignal<N>> stemSignals) {

        Map<Class, List<NetworkSignal<N>>> signalsByClass = groupSignalsByClass(stemSignals);
        double totalScore = calculateSignalsScore(signalsByClass);

        return totalScore;
    }

    private double calculateSignalsScore(Map<Class, List<NetworkSignal<N>>> signalsByClass) {

        double totalScore = 0.0;
        Set<SignalCategory> categories = new HashSet();
        for (Map.Entry<Class, List<NetworkSignal<N>>> entry : signalsByClass.entrySet()) {
            Class clazz = entry.getKey();
            List<NetworkSignal<N>> signals = entry.getValue();
            double classWeight = weightFunctionsByClass.get(clazz).apply((double)signals.size());
            totalScore += classWeight;
            categories.add(signals.get(0).getCategory());
        }

        return totalScore;
    }

    protected abstract Map<Class, Function<Double, Double>> createWeightFunctionsBySignal();

    private Map<Class, List<NetworkSignal<N>>> groupSignalsByClass(List<NetworkSignal<N>> signals) {

        Map<Class, List<NetworkSignal<N>>> signalsByClass = new HashMap<>();
        for (NetworkSignal signal : signals) {
            List<NetworkSignal<N>> signalsList = signalsByClass.get(signal.getClass());
            if (signalsList == null) {
                signalsList = new ArrayList<>();
                signalsByClass.put(signal.getClass(), signalsList);
            }
            signalsList.add(signal);
        }
        return signalsByClass;
    }

}
