package me.relevante.nlp;

import me.relevante.network.Network;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public abstract class AbstractNetworkStemUserIndex<N extends Network> implements NetworkStemUserIndex<N> {

    @Id
    private ObjectId id;
    private String stem;
    private String userId;
    private double score;

    public AbstractNetworkStemUserIndex(String stem,
                                        String userId,
                                        double score) {
        this.stem = stem;
        this.userId = userId;
        this.score = score;
    }

    public String getStem() {
        return stem;
    }

    public String getUserId() {
        return userId;
    }

    public double getScore() {
        return score;
    }
}
