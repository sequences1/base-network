package me.relevante.nlp;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;

/**
 * @author daniel-ibanez
 */
public interface NetworkSearchUserCreator<N extends Network, S extends NetworkSearchUser<N, ?, ?>> extends NetworkEntity<N> {
    S create(String userId);
}
