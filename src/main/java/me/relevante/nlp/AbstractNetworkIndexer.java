package me.relevante.nlp;

import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;
import me.relevante.nlp.core.Feature;
import me.relevante.persistence.NetworkIndexRepo;
import me.relevante.persistence.NetworkSearchUserRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractNetworkIndexer<N extends Network, S extends NetworkSearchUser<N, ?, ?>, I extends NetworkStemUserIndex<N>>
        implements NetworkIndexer<N> {

    protected final NetworkSearchUserCreator<N, S> searchUserCreator;
    protected final NetworkVectorizer<N> vectorizer;
    protected final NetworkIndexRepo<N, I> indexRepo;
    protected final NetworkSearchUserRepo<N, S> searchUserRepo;

    public AbstractNetworkIndexer(final NetworkSearchUserCreator<N, S> searchUserCreator,
                                  final NetworkVectorizer<N> vectorizer,
                                  final NetworkIndexRepo<N, I> indexRepo,
                                  final NetworkSearchUserRepo<N, S> searchUserRepo) {
        this.searchUserCreator = searchUserCreator;
        this.vectorizer = vectorizer;
        this.indexRepo = indexRepo;
        this.searchUserRepo = searchUserRepo;
    }

    @Override
    public void index(String userId) {
        S searchUser = searchUserCreator.create(userId);
        Map<String, Feature> userVector = vectorizer.generateVector(searchUser.getSignals());
        List<I> indexEntries = new ArrayList<>();
        for (Feature feature : userVector.values()) {
            I indexEntry = createIndexEntry(feature.getStem(), searchUser.getId(), feature.getScore());
            indexEntries.add(indexEntry);
        }
        searchUserRepo.save(searchUser);
        indexRepo.deleteByUserId(userId);
        indexRepo.save(indexEntries);
    }

    protected abstract I createIndexEntry(String stem, String userId, double score);

}
