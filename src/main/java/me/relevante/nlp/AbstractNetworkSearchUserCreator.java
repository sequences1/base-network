package me.relevante.nlp;

import me.relevante.model.NetworkFullPost;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkPostAction;
import me.relevante.model.NetworkSearchUser;
import me.relevante.model.NetworkSignal;
import me.relevante.model.NetworkSignalExtractor;
import me.relevante.network.Network;
import me.relevante.persistence.NetworkFullPostRepo;
import me.relevante.persistence.NetworkPostActionRepo;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractNetworkSearchUserCreator<N extends Network, U extends NetworkFullUser<N, ?>, P extends NetworkFullPost<N, ?>,
        S extends NetworkSearchUser<N, U, P>, L extends NetworkPostAction<N>, C extends NetworkPostAction<N>> implements NetworkSearchUserCreator<N, S> {

    protected NetworkSignalExtractor<N, U, P> signalExtractor;
    protected NetworkVectorizer<N> vectorizer;
    protected CrudRepository<U, String> fullUserRepo;
    protected NetworkFullPostRepo<P> fullPostRepo;
    protected NetworkPostActionRepo<N, L> likesRepo;
    protected NetworkPostActionRepo<N, C> commentsRepo;

    public AbstractNetworkSearchUserCreator(NetworkSignalExtractor<N, U, P> signalExtractor,
                                            NetworkVectorizer<N> vectorizer,
                                            CrudRepository<U, String> fullUserRepo,
                                            NetworkFullPostRepo<P> fullPostRepo,
                                            NetworkPostActionRepo<N, L> likesRepo,
                                            NetworkPostActionRepo<N, C> commentsRepo) {
        this.signalExtractor = signalExtractor;
        this.vectorizer = vectorizer;
        this.fullUserRepo = fullUserRepo;
        this.fullPostRepo = fullPostRepo;
        this.likesRepo = likesRepo;
        this.commentsRepo = commentsRepo;
    }

    @Override
    public S create(String userId) {
        U fullUser = fullUserRepo.findOne(userId);
        List<P> fullPosts = fullPostRepo.findByPostAuthorId(userId);
        List<L> likes = likesRepo.findByAuthorId(userId);
        List<C> comments = commentsRepo.findByAuthorId(userId);
        Map<String, P> fullPostMap = new HashMap<>();
        fullPosts.forEach(fullPost -> fullPostMap.put(fullPost.getId(), fullPost));
        for (L like : likes) {
            P fullPost = fullPostRepo.findOne(like.getPostId());
            fullPostMap.put(fullPost.getId(), fullPost);
        }
        for (C comment : comments) {
            P fullPost = fullPostRepo.findOne(comment.getPostId());
            fullPostMap.put(fullPost.getId(), fullPost);
        }
        List<NetworkSignal<N>> allSignals = new ArrayList<>();
        allSignals.addAll(signalExtractor.extract(fullUser));
        addLikesAndCommentsToFullPosts(likes, comments, fullPostMap);
        for (P fullPost : fullPostMap.values()) {
            List<NetworkSignal<N>> signals = signalExtractor.extract(fullPost);
            for (NetworkSignal<N> signal : signals) {
                if (signal.getRelatedUserId().equals(userId)) {
                    allSignals.add(signal);
                }
            }
        }
        addCustomSignals(fullUser, allSignals);
        S searchUser = createSearchUser(fullUser, allSignals);
        return searchUser;
    }

    protected abstract void addCustomSignals(U fullUser, List<NetworkSignal<N>> allSignals);

    protected abstract void addLikesAndCommentsToFullPosts(List<L> likes, List<C> comments, Map<String, P> fullPosts);

    protected abstract S createSearchUser(U fullUser, List<NetworkSignal<N>> signals);

}
