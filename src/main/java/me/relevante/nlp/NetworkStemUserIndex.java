package me.relevante.nlp;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

public interface NetworkStemUserIndex<N extends Network> extends NetworkEntity<N> {
    String getStem();
    String getUserId();
    double getScore();
}
