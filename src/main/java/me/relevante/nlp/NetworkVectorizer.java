package me.relevante.nlp;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkSignal;
import me.relevante.network.Network;
import me.relevante.nlp.core.Feature;

import java.util.List;
import java.util.Map;

public interface NetworkVectorizer<N extends Network> extends NetworkEntity<N> {
    Map<String, Feature> generateVector(List<NetworkSignal<N>> signals);
}
