package me.relevante.nlp;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

public interface NetworkIndexer<N extends Network> extends NetworkEntity<N> {
    void index(String userId);
}
