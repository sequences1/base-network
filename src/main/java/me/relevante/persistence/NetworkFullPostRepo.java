package me.relevante.persistence;

import me.relevante.model.NetworkFullPost;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkFullPostRepo<P extends NetworkFullPost> extends CrudRepository<P, String> {
    List<P> findByPostAuthorId(String authorId);
}
