package me.relevante.persistence;

import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkSearchUserRepo<N extends Network, S extends NetworkSearchUser<N, ?, ?>> extends CrudRepository<S, String> {
}
