package me.relevante.persistence;

import me.relevante.model.NetworkPostAction;
import me.relevante.network.Network;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkPostActionRepo<N extends Network, A extends NetworkPostAction<N>> extends CrudRepository<A, String> {
    List<A> findByAuthorId(String authorId);
}
