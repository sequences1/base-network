package me.relevante.persistence;

import me.relevante.network.Network;
import me.relevante.nlp.NetworkStemUserIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkIndexRepo<N extends Network, I extends NetworkStemUserIndex<N>> extends CrudRepository<I, String> {
    void deleteByUserId(String userId);
}
