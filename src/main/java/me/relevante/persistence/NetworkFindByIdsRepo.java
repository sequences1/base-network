package me.relevante.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.List;

/**
 * @author daniel-ibanez
 */
@NoRepositoryBean
public interface NetworkFindByIdsRepo<T> extends CrudRepository<T, String> {
    List<T> findByIdIn(Collection<String> ids);
}
