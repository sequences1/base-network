package me.relevante.model;

import me.relevante.network.Network;
import me.relevante.nlp.core.NlpEntity;

import java.util.Date;
import java.util.List;

public interface NetworkSignal<N extends Network> extends NlpEntity, NetworkEntity<N> {
    String getKey();
    String getRelatedUserId();
    void setRelatedUserId(String id);
    String getRelatedEntityId();
    SignalCategory getCategory();
    Date getSignalTimestamp();
    List<String> getContentStems();
}
