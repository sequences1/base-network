package me.relevante.model;

import me.relevante.network.Network;

public interface NetworkEntity<N extends Network> {
    N getNetwork();
}
