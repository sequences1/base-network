package me.relevante.model;

import me.relevante.network.Network;
import org.bson.types.ObjectId;

import java.util.Date;

public interface NetworkAction<N extends Network> extends NetworkEntity<N> {

    ObjectId getId();
    String getAuthorId();
    ActionStatus getStatus();
    ActionResult getResult();
    Date getCreationTimestamp();
    Date getProcessedTimestamp();

    void setSuccess();
    void setError();
    void setAlreadySucceeded();
    void setForbidden();
    void setUserManaged();
}