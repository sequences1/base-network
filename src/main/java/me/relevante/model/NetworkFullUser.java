package me.relevante.model;

import me.relevante.network.Network;

import java.util.List;

public interface NetworkFullUser<N extends Network, P extends NetworkProfile<N, ?>> extends NetworkUser<N> {
    String getId();
    P getProfile();
    List<String> getRelatedTerms();
}
