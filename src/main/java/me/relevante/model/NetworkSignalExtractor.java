package me.relevante.model;

import me.relevante.network.Network;

import java.util.List;

public interface NetworkSignalExtractor<N extends Network, U extends NetworkFullUser<N, ?>, P extends NetworkFullPost<N, ?>> extends NetworkEntity<N> {
    List<NetworkSignal<N>> extract(U fullUser);
    List<NetworkSignal<N>> extract(P fullPost);
}
