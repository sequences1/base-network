package me.relevante.model;

import me.relevante.network.Network;

public interface NetworkFullPost<N extends Network, P extends NetworkPost<N, ?, ?>> extends NetworkEntity<N> {
    String getId();
    P getPost();
}
