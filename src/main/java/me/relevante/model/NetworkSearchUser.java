package me.relevante.model;

import me.relevante.network.Network;

import java.util.List;

public interface NetworkSearchUser<N extends Network, U extends NetworkFullUser<N, ?>, P extends NetworkFullPost<N, ?>> extends NetworkUser<N> {
    String getId();
    U getFullUser();
    void setFullUser(U fullUser);
    List<NetworkSignal<N>> getSignals();
    void addSignal(NetworkSignal<N> signal);
    List<P> getPosts();
}
