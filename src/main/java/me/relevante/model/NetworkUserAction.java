package me.relevante.model;

import me.relevante.network.Network;

public interface NetworkUserAction<N extends Network> extends NetworkAction<N> {

    String getTargetUserId();

}
