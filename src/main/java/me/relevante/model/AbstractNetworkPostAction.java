package me.relevante.model;

import me.relevante.network.Network;

public abstract class AbstractNetworkPostAction<N extends Network> extends AbstractNetworkAction<N> implements NetworkPostAction<N> {

    protected String postId;

    public AbstractNetworkPostAction() {
        super();
    }

    public AbstractNetworkPostAction(String authorId,
                                     String postId) {
        super(authorId);
        this.postId = postId;
    }

    @Override
    public String getPostId() {
        return postId;
    }
}