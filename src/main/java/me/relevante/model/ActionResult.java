package me.relevante.model;

public enum ActionResult {

    ALREADY_SUCCEEDED,
    ERROR,
    FORBIDDEN,
    SUCCESS,
    USER_MANAGED

}
