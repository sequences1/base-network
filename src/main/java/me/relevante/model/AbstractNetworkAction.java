package me.relevante.model;

import me.relevante.network.Network;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Date;

public abstract class AbstractNetworkAction<N extends Network> implements NetworkAction<N> {

    @Id
    protected ObjectId objectId;
    protected String authorId;
    protected ActionStatus status;
    protected ActionResult result;
    protected Date creationTimestamp;
    protected Date processedTimestamp;

    public AbstractNetworkAction() {
        this.creationTimestamp = new Date();
        this.status = ActionStatus.IN_PROGRESS;
    }

    public AbstractNetworkAction(String authorId) {
        this();
        this.authorId = authorId;
    }

    @Override
    public ObjectId getId() {
        return objectId;
    }

    @Override
    public String getAuthorId() {
        return authorId;
    }

    @Override
    public ActionStatus getStatus() {
        return status;
    }

    @Override
    public ActionResult getResult() {
        return result;
    }

    @Override
    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    @Override
    public Date getProcessedTimestamp() {
        return processedTimestamp;
    }

    @Override
    public void setSuccess() {
        this.result = ActionResult.SUCCESS;
        this.status = ActionStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setError() {
        this.result = ActionResult.ERROR;
        this.status = ActionStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setAlreadySucceeded() {
        this.result = ActionResult.ALREADY_SUCCEEDED;
        this.status = ActionStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setForbidden() {
        this.result = ActionResult.FORBIDDEN;
        this.status = ActionStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

    @Override
    public void setUserManaged() {
        this.result = ActionResult.USER_MANAGED;
        this.status = ActionStatus.PROCESSED;
        this.processedTimestamp = new Date();
    }

}