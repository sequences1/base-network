package me.relevante.model;

import me.relevante.network.Network;
import me.relevante.nlp.core.NlpEntity;

public interface NetworkProfile<N extends Network, P extends NetworkProfile<N, ?>> extends NetworkUser<N>, NlpEntity, Updatable<P> {
    String getId();
}
