package me.relevante.model;

import me.relevante.network.Network;

public interface NetworkUser<N extends Network> extends NetworkEntity<N> {

    String getUserId();

    default boolean equals(NetworkUser<N> networkUser) {
        if (networkUser == null) {
            return false;
        }
        if (!this.getNetwork().equals(networkUser.getNetwork())) {
            return false;
        }
        if (!this.getUserId().equals(networkUser.getUserId())) {
            return false;
        }
        return true;
    }
}
