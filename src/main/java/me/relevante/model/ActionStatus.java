package me.relevante.model;

public enum ActionStatus {

    IN_PROGRESS,
    PROCESSED

}
