package me.relevante.model;

public enum SignalCategory {
    SELF_DESCRIPTION,
    CONTENT_ACTIVITY,
    OWNERSHIP,
    NETWORK
}
