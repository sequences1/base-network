package me.relevante.model;

import me.relevante.network.Network;

import java.util.List;

public interface NetworkPostsContainer<N extends Network, P extends NetworkFullPost<N, ?>> {
    List<P> getLastPosts();
}
