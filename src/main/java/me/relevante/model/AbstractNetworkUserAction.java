package me.relevante.model;

import me.relevante.network.Network;

public abstract class AbstractNetworkUserAction<N extends Network> extends AbstractNetworkAction<N> implements NetworkUserAction<N> {

    protected String targetUserId;

    public AbstractNetworkUserAction() {
        super();
    }

    public AbstractNetworkUserAction(String authorId,
                                     String targetUserId) {
        super(authorId);
        this.targetUserId = targetUserId;
    }

    @Override
    public String getTargetUserId() {
        return targetUserId;
    }
}