package me.relevante.model;

import me.relevante.network.Network;

public interface NetworkPostAction<N extends Network> extends NetworkAction<N> {

    String getPostId();

}